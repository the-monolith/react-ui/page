import { component, React, Style } from 'local/react/component'
import { state } from 'local/react/state'

import * as classes from './classes.css'

const join = (...names: string[]) => names.filter(x => x).join(' ')

export const defaultBackdropStyle: Style = {
  position: 'absolute',
  top: '0',
  left: '0',
  right: '0',
  bottom: '0',
  overflow: 'auto'
}

export const defaultPageStyle: Style = {
  width: '600px',
  margin: '1em auto',
  padding: '0.5em 1em',
  border: '1px solid #ccc',
  borderRadius: '1rem',
  maxWidth: 'calc(100% - 2em)',
  boxSizing: 'border-box',
  position: 'relative',
  overflow: 'hidden'
}

const maskingState = state({
  visible: false,
  masked: true
})

setTimeout(() => {
  maskingState.visible = true
}, 300)

export const Page = component.children
  .props<{ className?: string; style?: Style; backdropStyle?: Style }>({
    backdropStyle: defaultBackdropStyle,
    style: defaultPageStyle
  })
  .render(({ children, backdropStyle, ...props }) => (
    <div
      style={backdropStyle}
      className={join(
        classes.page,
        maskingState.visible ? undefined : classes.hidden
      )}
    >
      <div {...props}>{children}</div>
    </div>
  ))
