import { React, render } from 'local/react/example'
import { Root } from 'local/react/state'

import { Page } from './index'

render(
  <Root
    render={() => <Page>Hello world, this is an example of a page.</Page>}
  />
)
